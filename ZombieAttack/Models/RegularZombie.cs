﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Observers;

namespace ZombieAttack
{
    class RegularZombie : ZombieSubject, Zombie
    {
        public int ZombieHP = 0;
        public string ZombieType { get; set; }
        public bool IsDead { get; set; }
        public Accessory ZombieShield { get; set; }
        public event EventHandler ZombieDied;

        public int HP
        {
            get
            {
                return this.ZombieHP;
            }

            //Automatically calls Die() on the object if pre-existing HP was set above 0,
            //zombie is not currently destroyed, and value being passed in is 0 or less. 
            set
            {
                if (this.ZombieHP > 0 && !this.IsDead && value <= 0)
                {
                    this.Die();
                }
                else
                {
                    ZombieHP = value;
                }
            }
        }        
        

        public RegularZombie(int HP, bool isDead, string ZombieType)
        {
            this.HP = HP;
            this.IsDead = isDead;
            this.ZombieType = ZombieType;
        }

        public int TakeDamage(int Damage)
        {
            //The setter of HP already knows how to handle death.. just decrement damage, then.
            HP -= Damage;

            return HP;
        }

        public void Die()
        {
            ZombieHP = 0;
            IsDead = true;

            //Notify observers of this zombies death.
            NotifyAllObservers();

        }

        public Accessory GetAccessory()
        {
            //Not necessary for regular zombies at this time.
            throw new NotImplementedException();
        }

        public int GetHP()
        {
            return this.HP;
        }

        public string GetZombieType()
        {
            return this.ZombieType;
        }

        public virtual void takeDamageFromAbove(int d)
        {
            this.TakeDamage(d);
        }
    }
}
