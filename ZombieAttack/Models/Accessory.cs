﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack
{
    public class Accessory
    {
        private int AccessoryHP = 0;
        public bool isMetal;

        public int HP {
            get
            {
                return this.AccessoryHP;
            }

            //Automatically calls destruct on the object if pre-existing HP was set above 0,
            //accesory is not currently destroyed, and value being passed in is 0 or less. 
            set
            {                     
                   if(this.AccessoryHP > 0 && !this.IsDestroyed && value <= 0)
                    {
                        this.Destruct();
                    }
                   else
                    {
                        this.AccessoryHP = value;
                    }
                }
        }
        public string Description { get; private set; }
        public bool IsDestroyed { get; set; }      


        public Accessory(int HP, string Description, bool isMetal)
            {
                this.HP = HP;
                this.Description = Description;
                this.IsDestroyed = false;
                this.isMetal = isMetal;
            }

        //Sets the accessories internal state to destroyed.
        public void Destruct()
        {
            this.AccessoryHP = 0;
            this.IsDestroyed = true;
        }




    }
}
