﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Decorators;

namespace ZombieAttack
{
    class ZombieFactory : ZombieEnemyFactory
    {
        public Zombie BuildNewZombie(int HP, bool IsDead, string ZombieType)
        {
            switch (ZombieType)
            {
                case "Cone":

                    return new ConeAccessoryDecorator(new RegularZombie(50, false, ZombieType));

                case "Bucket":

                    return new BucketAccessoryDecorator(new RegularZombie(50, false, ZombieType));

                case "ScreenDoor":

                    return new ScreenDoorAccessoryDecorator(new RegularZombie(50, false, ZombieType));

                case "Regular":

                    return new RegularZombie(HP, IsDead, ZombieType);

                default:
                    throw new ApplicationException(string.Format("Cannot create zombie {0}", ZombieType));

            }
        }
    }
}
