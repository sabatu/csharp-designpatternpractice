﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack.Observers
{
    public abstract class ZombieSubject
    {
        //subscribers
        private List<ZombieObserver> observers = new List<ZombieObserver>();

        public void attach(ZombieObserver observer)
        {
            observers.Add(observer);
        }

        public void detach(ZombieObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyAllObservers()
        {
            foreach(ZombieObserver observer in observers)
            {
                observer.ZombieDied();
            }
        }
    }
}
