﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack.Observers
{
    public abstract class ZombieObserver
    {
        public ZombieSubject subject;
        public abstract void ZombieDied();
    }
}

