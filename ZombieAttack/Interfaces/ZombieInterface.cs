﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Observers;

namespace ZombieAttack
{
    public interface Zombie 
    {
        int TakeDamage(int Damage);
        void Die();
        Accessory GetAccessory();
        int GetHP();      
        string GetZombieType();
        void takeDamageFromAbove(int d);
    }
}
