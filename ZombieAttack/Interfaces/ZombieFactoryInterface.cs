﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack
{
    interface ZombieEnemyFactory
    {   //All zombie factories must implement the following method.
        Zombie BuildNewZombie(int HP, bool IsDead, string ZombieType);
    }
}
