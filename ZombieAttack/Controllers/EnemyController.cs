﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Decorators;

namespace ZombieAttack.Controllers
{
    class EnemyController
    {
        GameObjectManager CurrentEnemies;
        ZombieFactory EnemyFactory;

        public EnemyController()
        {
            CurrentEnemies = new GameObjectManager();
            EnemyFactory = new ZombieFactory();
        }

        public void CreateRegularZombie()
        {
            RegularZombie newZombie = (RegularZombie)EnemyFactory.BuildNewZombie(50, false, "Regular");
            CurrentEnemies.AddSingleEnemy(newZombie);

            //Attach new zombie instance to its observer.
            newZombie.attach(CurrentEnemies);
        }

        public void CreateConeZombie()
        {
            ConeAccessoryDecorator newZombie = (ConeAccessoryDecorator)EnemyFactory.BuildNewZombie(50, false, "Cone");
            CurrentEnemies.AddSingleEnemy(newZombie);

            //Attach new zombie instance to its observer.
            newZombie.decoratedZombie.attach(CurrentEnemies);
        }

        public void CreateBucketZombie()
        {
            BucketAccessoryDecorator newZombie = (BucketAccessoryDecorator)EnemyFactory.BuildNewZombie(50, false, "Bucket");
            CurrentEnemies.AddSingleEnemy(newZombie);

            //Attach new zombie instance to its observer.
            newZombie.decoratedZombie.attach(CurrentEnemies);
        }

        public void CreateScreenDoorZombie()
        {
            ScreenDoorAccessoryDecorator newZombie = (ScreenDoorAccessoryDecorator)EnemyFactory.BuildNewZombie(50, false, "ScreenDoor");
            CurrentEnemies.AddSingleEnemy(newZombie);

            //Attach new zombie instance to its observer.
            newZombie.decoratedZombie.attach(CurrentEnemies);
        }





        public List<Zombie> ReturnCurrentEnemies()
        {
            return CurrentEnemies.ReturnEnemies();
        }

        public Zombie ReturnCurrentEnemy()
        {
            return CurrentEnemies.ReturnEnemies().First(); 
        }

        public void RemoveCurrentEnemy()
        {
            CurrentEnemies.RemoveFirstEnemy();
        }

        //Used for changing enemy type to regular on-the-fly.
        public void ChangeCurrentEnemyToRegular(int newHP)
        {
            RegularZombie newZombie = (RegularZombie)EnemyFactory.BuildNewZombie(newHP, false, "Regular");
            CurrentEnemies.ReturnEnemies().Insert(0, newZombie);

            //Attach new zombie instance to its observer.
            newZombie.attach(CurrentEnemies);

        }


        public int ReturnCurrentEnemiesCount()
        {
            return CurrentEnemies.ReturnEnemies().Count;
        }


    }
}
