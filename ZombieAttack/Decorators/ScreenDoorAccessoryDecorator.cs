﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack.Decorators
{
    class ScreenDoorAccessoryDecorator : AccessoryDecorator
    {
        public ScreenDoorAccessoryDecorator(RegularZombie ZombieToDecorate) : base(ZombieToDecorate)
        {
            ZombieToDecorate.ZombieShield = new Accessory(75, "ScreenDoor", true);
        }

        //The setter of the base regular zombie class knows how to handle damage taken directly to the zombies base HP, so all we have to do is call it.
        public override void takeDamageFromAbove(int damage)
        {
            decoratedZombie.HP -= damage;
        }
    }
}
