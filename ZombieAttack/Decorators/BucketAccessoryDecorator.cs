﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack.Decorators
{
    class BucketAccessoryDecorator : AccessoryDecorator
    {
        public BucketAccessoryDecorator(RegularZombie ZombieToDecorate) : base(ZombieToDecorate)
        {
            ZombieToDecorate.ZombieShield = new Accessory(100, "Bucket", true);
        }

        public override void takeDamageFromAbove(int d)
        {
            TakeDamage(d);
        }

    }
}
