﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack.Decorators
{
    class ConeAccessoryDecorator : AccessoryDecorator
    {
        public ConeAccessoryDecorator(RegularZombie ZombieToDecorate) : base(ZombieToDecorate)
        {
            ZombieToDecorate.ZombieShield = new Accessory(25, "Cone", false);
        }

        public override void takeDamageFromAbove(int d)
        {
            TakeDamage(d);
        }

    }
}
