﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAttack.Decorators
{
    abstract class AccessoryDecorator : Zombie
    {
        public RegularZombie decoratedZombie;
        

        public AccessoryDecorator(RegularZombie ZombieToDecorate)
        {
            this.decoratedZombie = ZombieToDecorate;
        }


        public int TakeDamage(int Damage)
        {
            //Check if the zombie's shield has already been destroyed.
            if (!decoratedZombie.ZombieShield.IsDestroyed)
            {
                //First, see if the damage is so catastrophic that it wipes out both the zombie and its accessory.
                if (Damage > (decoratedZombie.ZombieShield.HP + decoratedZombie.HP))
                {
                    decoratedZombie.ZombieShield.Destruct();
                    decoratedZombie.Die();
    }

                /*If damage is less then first case, check if damage is still greater than accessory.
                 * -Damage greater than remaining Accessory HP will be applied to zombie, and then Destruct()
                 * -gets called on Zombie's accessory, setting its isDestroyed state to true and it's interal HP
                 * -to 0, so there is no chance of it ever being used again.
                */
                else if (Damage > decoratedZombie.ZombieShield.HP)
                {
                    decoratedZombie.HP -= Damage - decoratedZombie.ZombieShield.HP;
                    decoratedZombie.ZombieShield.Destruct();

                }
                /*The only case left then is that the damage is less than or equal to the zombie's accessory.
                 *  -The accessory setter will automatically set the Accessory object's isDestroyed attribute
                 *  -to true and set accessory HP to 0 if damage is equivalent to remaining Accessory HP, so we
                 *  -can let it do it's thing in either case. 
                 * */
                else
                {
                    decoratedZombie.ZombieShield.HP -= Damage;
                }
}
            else
            {
                decoratedZombie.HP -= Damage;

                if (decoratedZombie.HP <= 0)
                {
                    decoratedZombie.Die();
                }
            }

            return decoratedZombie.HP;

        }


        public void Die()
        {
            decoratedZombie.ZombieHP = 0;
            decoratedZombie.IsDead = true;
        }


        public ZombieAttack.Accessory GetAccessory()
        {
            return decoratedZombie.ZombieShield;
        }


        public int GetHP()
        {
            return decoratedZombie.HP + decoratedZombie.ZombieShield.HP;
        }

        public string GetZombieType()
        {
            return decoratedZombie.ZombieType;
        }


        public abstract void takeDamageFromAbove(int d);


        //Accessorized zombies should still be able to communicate their base HP values.
        public int GetBaseHP()
        {
            return decoratedZombie.HP;
        }
  


    }
}
