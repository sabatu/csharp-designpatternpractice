﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Controllers;

namespace ZombieAttack
{
    class UserInterface
    {
        
            private int UserSelection;
            private string CurrentZombieList;
            private EnemyController ZombieController;
            private GameEventManager GEM;

            public UserInterface()
            {
                UserSelection = 0;
                CurrentZombieList = String.Empty;
                ZombieController = new EnemyController();
                GEM = new GameEventManager(ZombieController);
            }

            public void MainMenu()
            {

                Console.Clear();

                while (UserSelection != 3)
                {
                    Console.Clear();
                    Console.WriteLine("1. Create Zombies?");
                    Console.WriteLine("2. Demo game play?");
                    Console.WriteLine("3. Exit?");


                    //Print enemy list, if it exists.
                    if(ZombieController.ReturnCurrentEnemiesCount() > 0)
                    {
                        PrintZombieArray();
                    }

                    UserSelection = int.Parse(Console.ReadLine());

                    if(UserSelection == 1)
                    {
                        AddZombies();
                    }
                    else if (UserSelection == 2)
                    {
                        AttackEnemies();
                    }
                }
            }


            //Add zombies to the list.
            public void AddZombies()
            {
                int AddZombieChoice = 0;

                Console.Clear();

                while (AddZombieChoice != 5)
                {
                    Console.WriteLine("Which Kind?");

                    Console.WriteLine("1. Regular");
                    Console.WriteLine("2. Cone");
                    Console.WriteLine("3. Bucket");
                    Console.WriteLine("4. Screen Door");
                    Console.WriteLine("5. Return To Main Screen" + Environment.NewLine);

                    AddZombieChoice = int.Parse(Console.ReadLine());

                    if (AddZombieChoice == 1)
                    {
                        ZombieController.CreateRegularZombie();                      
                        Console.WriteLine("Regular Zombie Added To List." + Environment.NewLine);
                    }
                    else if(AddZombieChoice == 2)
                    {
                        ZombieController.CreateConeZombie();
                        Console.WriteLine("Cone Zombie Added To List." + Environment.NewLine);
                    }
                    else if (AddZombieChoice == 3)
                    {
                        ZombieController.CreateBucketZombie();
                        Console.WriteLine("Bucket Zombie Added To List." + Environment.NewLine);
                    }
                    else if (AddZombieChoice == 4)
                    {
                        ZombieController.CreateScreenDoorZombie();
                        Console.WriteLine("Screen Door Zombie Added To List." + Environment.NewLine);
                    }
                }
            }

            public void AttackEnemies()
            {
                int AttackChoice = 0;

                Console.Clear();
                Console.WriteLine("Welcome to attack mode!");
                PrintZombieArray();
                Console.WriteLine(Environment.NewLine);

                while (AttackChoice != -1)
                {
                    
                    Console.WriteLine("Enter 1 to attack with a Peashooter," + "\n" +
                                      "2 to attack with a watermelon," + "\n" +
                                      "3 to attack with a Magnet-shroom," + "\n" +
                                      " or enter -1 to return to the main screen.");

                    AttackChoice = int.Parse(Console.ReadLine());

                    if(AttackChoice > 0 && ZombieController.ReturnCurrentEnemiesCount() < 1)
                    {
                    Console.WriteLine("Sorry, there are no zombies to attack. Please return to the main menu and add in some zombies first..");
                    }
                    else if(AttackChoice > 0 && AttackChoice < 4)
                    {
                        int ZombieCount = ZombieController.ReturnCurrentEnemiesCount();

                        Zombie CurrentZombieToAttack = ZombieController.ReturnCurrentEnemy();
                        GEM.simulateCollisionDetection(AttackChoice);
                   
                        //Did zombie get killed in the last attack? Search for its existence in the object manager.
                        if(ZombieCount > ZombieController.ReturnCurrentEnemiesCount())
                        {
                            Console.WriteLine(Environment.NewLine + "The " + CurrentZombieToAttack.GetZombieType() + " Zombie has been killed." + "\n");                                                  
                        }

                        //If Zombie HP is below 50 and not a regular zombie type, then replace zombie with an instance of regular zombie.
                        else if(CurrentZombieToAttack.GetHP() < 50 && CurrentZombieToAttack.GetZombieType() != "Regular")
                        {
                            ZombieController.RemoveCurrentEnemy();
                            ZombieController.ChangeCurrentEnemyToRegular(CurrentZombieToAttack.GetHP());
                        }
                    }

                    PrintZombieArray();
                }
            }

            private void PrintZombieArray()
            {
                CurrentZombieList = Environment.NewLine + "|";

                foreach (Zombie zombie in ZombieController.ReturnCurrentEnemies())
                {
                    CurrentZombieList += zombie.GetZombieType().ElementAt(0) + "/" + zombie.GetHP().ToString() + "| ";
                }

                CurrentZombieList += "|" + Environment.NewLine;

                Console.WriteLine(CurrentZombieList);

            }
        }
    }
    

