﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Observers;

namespace ZombieAttack
{
    class GameObjectManager : ZombieObserver
    {
        //Keep track of all of the enemies.
        //Accessed by GameEventManager when calculating collision.
        //The list here is filled when the user creates the Zombies.
        private List<Zombie> enemies;

        public GameObjectManager()
        {
            enemies = new List<Zombie>();
        }

        public List<Zombie> ReturnEnemies()
        {
            return enemies;
        }

        public void AddSingleEnemy(Zombie ZombieToAdd)
        {
            enemies.Add(ZombieToAdd);
        }

        public void AddListOfEnemies(List<Zombie> ZombiesToAdd)
        {
            enemies.AddRange(ZombiesToAdd);
        }

        public void RemoveFirstEnemy()
        {
            enemies.RemoveAt(0);
        }

 
        public override void ZombieDied()
        {
            enemies.RemoveAt(0);
        }
    }
}
