﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAttack.Controllers;
using ZombieAttack.Decorators;

namespace ZombieAttack
{
    class GameEventManager
    {
        ZombieFactory EnemyFactory = new ZombieFactory();
        EnemyController CurrentZombieController;

        public GameEventManager(EnemyController ZombieController)
        {
            this.CurrentZombieController = ZombieController;
        }



        public void doDamage(int d, Zombie e)
        {
            e.TakeDamage(d);
        }

        public void doDamageFromAbove(int d, Zombie e)
        { 
            e.takeDamageFromAbove(d);  
        }

        //Called when "collision" is detected between
        //a magnet-shroom and an Enemy e
        //i.e, when the user select the magnet-shroom attack.
        public void applyMagnetForce(Zombie e)
        {
            if ((e.GetType() == typeof(BucketAccessoryDecorator) || e.GetType() == typeof(ScreenDoorAccessoryDecorator)) && e.GetAccessory().isMetal)
            {
                //At this point, we know this is an accessorized zombie. Therefore, we want to make sure we're getting the accessorized zombie's actual base HP
                //(For instance, if this is a screen door zombie that's already had its base HP damaged by a watermelon, we need to know this when we turn it into a regular zombie!)
                AccessoryDecorator ZombieToConvert = (AccessoryDecorator)e;

                CurrentZombieController.RemoveCurrentEnemy();
                CurrentZombieController.ChangeCurrentEnemyToRegular(ZombieToConvert.GetBaseHP());
            }

        }

        //To separate the responsibilities, the above methods should not 
        //be called directly from your code handling user-interaction. 
        //Instead, it should be done in this “hub” operation in the control 
        //class. Since we are simulating, pass an “int” to represent the plant. 
        public void simulateCollisionDetection(int plant)
        {
            Zombie EnemyToAttack = CurrentZombieController.ReturnCurrentEnemy();

            switch(plant)
            {
                //Peashooters (direct damage.)
                case 1:

                    this.doDamage(25, CurrentZombieController.ReturnCurrentEnemy());
                    break;

                //Watermelons (damage from above, which negates screen doors.)
                case 2:

                    this.doDamageFromAbove(30, CurrentZombieController.ReturnCurrentEnemy());
                    break;


                //Magnet-shrooms (which remove metal accessories like buckets and screen doors.)
                case 3:

                    this.applyMagnetForce(CurrentZombieController.ReturnCurrentEnemy());
                    break;

                default:
                    throw new ApplicationException(string.Format("Cannot create plant {0}", plant));
            }

        }

    }
}
